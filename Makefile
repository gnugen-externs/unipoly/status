all: unipoly.pdf

unipoly.pdf: unipoly.tex
	lualatex unipoly.tex

clean:
	rm unipoly.pdf

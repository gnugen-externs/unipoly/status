# Status UNIPOLY

Ce dépôt contient les status d'[UNIPOLY](unipoly.epfl.ch).

## Générer un fichier PDF depuis les sources LaTeX

Il faut avoir le programme `lualatex` ainsi qu'un certain nombre de paquets
LaTeX installés sur votre système. Le programme `make` est optionel et ne sert
qu'à appeler une commande prédéfinie dans le fichier `Makefile`.

Le plus simple est d'installer le meta-paquet LaTeX de votre distribution, qui
installera l'ensemble des dépendences requises d'un coup. Cela correspond à:
  * `texlive-schema-full` pour [Fedora](https://getfedora.org/en/): `sudo dnf install texlive-schema-full make` (incluant `make`)
  * `texlive-full` pour [Debian](https://www.debian.org/) et [Ubuntu](https://www.ubuntu.com/): `sudp apt update; sudo apt install texlive-full make` (incluant `make`)

*N'hésitez pas à mettre à jour ce README avec les détails concernant votre
système. L'installation est généralement triviale sous toute distribution
GNU/Linux.*

Il suffit ensuite de lancer (dans un terminal) la commande `make` à la racine
du dépôt. Le fichier PDF se trouvera sous `unipoly.pdf` si tout se déroule
correctement.
